from bpp.util import train_test_split
import numpy as np


def mock_data():
    return np.random.rand(10, 24, 1)  # 10 windows, 24 items in each, 1 item value


def test_train_test_split():
    data = mock_data()
    X_train, X_test, y_train, y_test = train_test_split(data=data, test_size=0.2)
    assert X_train.shape == (8, 23, 1)
    assert X_test.shape == (2, 23, 1)
    assert y_train.shape == (8, 1)
    assert y_test.shape == (2, 1)


def test_train_test_split_test_size_0():
    data = mock_data()
    X_train, X_test, y_train, y_test = train_test_split(data=data, test_size=0)
    assert X_train.shape == (10, 23, 1)
    assert X_test.shape == (0, 23, 1)
    assert y_train.shape == (10, 1)
    assert y_test.shape == (0, 1)


def test_train_test_split_test_size_1():
    data = mock_data()
    X_train, X_test, y_train, y_test = train_test_split(data=data, test_size=1)
    assert X_train.shape == (0, 23, 1)
    assert X_test.shape == (10, 23, 1)
    assert y_train.shape == (0, 1)
    assert y_test.shape == (10, 1)
