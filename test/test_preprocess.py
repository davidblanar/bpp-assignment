from bpp.preprocess import Preprocess
import pandas as pd
import numpy as np


class MockScaler:
    def fit_transform(self, x):
        x[True] = 1.0
        return x


class EmptyScaler:
    def fit_transform(self, x):
        return x


def test_create_windows():
    scaler = MockScaler()
    preprocess = Preprocess(scaler=scaler)
    df = pd.DataFrame({
        'data': list(range(6))
    })
    windowed = preprocess.create_windows(df, 3)
    expected = [
        [[0], [1], [2]],
        [[1], [2], [3]],
        [[2], [3], [4]],
        [[3], [4], [5]]
    ]
    assert np.array_equal(np.array(expected), windowed)


def test_encode_date():
    scaler = MockScaler()
    preprocess = Preprocess(scaler=scaler)
    df = pd.DataFrame({
        'Time Stamp': ['01/01/2017 00:00']
    })
    encoded = preprocess.encode_date(df)
    assert encoded.shape == (1, 2)
    assert encoded.to_dict(orient='records') == [
        {
            'Time Stamp': '01/01/2017 00:00',
            'time_stamp': pd.Timestamp('2017-01-01 00:00:00')
        }
    ]


def test_scale_data():
    scaler = MockScaler()
    preprocess = Preprocess(scaler=scaler)
    df = pd.DataFrame({
        'LBMP ($/MWHr)': [27.05, 33.1],
    })
    result = preprocess.scale_data(df)
    assert np.array_equal(result, np.array([[1.0], [1.0]]))


def test_preprocess():
    scaler = EmptyScaler()
    preprocess = Preprocess(scaler=scaler)
    df = pd.DataFrame({
        'LBMP ($/MWHr)': list(range(12)),
        'Name': ['N.Y.C.' if i % 2 == 0 else 'other' for i in list(range(12))],
        'Time Stamp': ['01/01/2017 00:00', '01/01/2017 00:00', '01/01/2017 01:00', '01/01/2017 01:00',
                       '01/01/2017 02:00', '01/01/2017 02:00', '01/01/2017 03:00', '01/01/2017 03:00',
                       '01/01/2017 04:00', '01/01/2017 04:00', '01/01/2017 05:00', '01/01/2017 05:00']
    })
    result = preprocess.run(df, window_size=3, load_zone='N.Y.C.')
    expected = [
        [[0], [2], [4]],
        [[2], [4], [6]],
        [[4], [6], [8]],
        [[6], [8], [10]]
    ]
    assert np.array_equal(np.array(expected), result)
