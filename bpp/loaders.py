import pandas as pd
import glob


class PandasGlobLoader:
    def __init__(self, pattern):
        self.pattern = pattern

    def load(self):
        files = glob.glob(self.pattern)
        return pd.concat((pd.read_csv(f) for f in files), ignore_index=True)
