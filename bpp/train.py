import torch
from util import train_test_split
from model import GRU
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import argparse
import logging
import pickle

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
logging.getLogger('matplotlib.font_manager').setLevel(logging.ERROR)


class Model:
    def __init__(self, X, y, predictive=None, scaler=None, figures_folder=None, input_size=1, hidden_size=64,
                 num_layers=2, output_size=1, epochs=100, learning_rate=0.01):
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        logging.info(f'Using device: {device}')
        self.device = torch.device(device)
        self.model = GRU(input_size=input_size, hidden_size=hidden_size, output_size=output_size, num_layers=num_layers)
        self.criterion = torch.nn.MSELoss(reduction='mean')
        self.optimiser = torch.optim.Adam(self.model.parameters(), lr=learning_rate)
        self.epochs = epochs
        self.X = X
        self.y = y
        self.losses = []
        self.predictive = predictive
        self.figures_folder = figures_folder
        self.scaler = scaler

    def fit(self):
        logging.info('Training data')
        X = torch.from_numpy(self.X).to(self.device).type(torch.Tensor)
        y = torch.from_numpy(self.y).to(self.device).type(torch.Tensor)
        for epoch in range(self.epochs):
            y_pred = self.model(X)
            loss = self.criterion(y_pred, y)
            logging.info(f'Epoch {epoch + 1}/{self.epochs}, loss: {loss.item()}')
            self.losses.append(loss.item())
            self.optimiser.zero_grad()
            loss.backward()
            self.optimiser.step()
        logging.info('Training done')

    def evaluate(self, X, y):
        X = torch.from_numpy(X).to(self.device).type(torch.Tensor)
        y = torch.from_numpy(y).to(self.device).type(torch.Tensor)
        with torch.no_grad():
            prediction = self.model(X)
            evaluation = pd.DataFrame(prediction.numpy())
            evaluation.columns = ['predicted']
            evaluation['actual'] = y.numpy()
            evaluation['error'] = evaluation.apply(lambda x: abs(abs(x['predicted']) - abs(x['actual'])), axis=1)
            plt.plot(prediction)
            plt.plot(y)
            plt.legend(['Prediction', 'Actual'])
            if self.figures_folder is None:
                plt.show()
            else:
                plt.savefig(f'{self.figures_folder}/evaluation.png')
                plt.close()
            logging.info(f'Error metrics:')
            logging.info(evaluation['error'].describe())

    def plot_loss(self):
        if len(self.losses) == 0:
            raise Exception('Train model first')
        plt.plot(self.losses)
        plt.legend(['Loss'])
        if self.figures_folder is None:
            plt.show()
        else:
            plt.savefig(f'{self.figures_folder}/loss_plot.png')
            plt.close()

    def predict(self, data):
        if self.predictive is None:
            raise Exception('No predictive')
        predictions = self.predictive.model(data).detach().numpy()
        if self.scaler is not None:
            predictions = self.scaler.inverse_transform(predictions)
        return predictions


def main():
    parser = argparse.ArgumentParser(description='Training')
    parser.add_argument('--data-path', help='Path to preprocessed data', required=False,
                        default='./preprocessed/data.npy')
    parser.add_argument('--output-path', help='Model output path', required=False,
                        default='./model/model.pickle')
    parser.add_argument('--figures-folder', help='Path to folder to store figures', required=False,
                        default='./figures')
    parser.add_argument('--hidden-size', help='Hidden layer size', required=False, default=64, type=int)
    parser.add_argument('--num-layers', help='Number of layers', required=False, default=2, type=int)
    parser.add_argument('--epochs', help='Number of training epochs', required=False, default=150, type=int)
    parser.add_argument('--learning-rate', help='Learning rate', required=False, default=0.002, type=float)
    parser.add_argument('--test-size', help='Size of test set', required=False, default=0.2, type=float)
    args = parser.parse_args()

    data = np.load(args.data_path)
    X_train, X_test, y_train, y_test = train_test_split(data=data, test_size=args.test_size)
    model = Model(
        X=X_train,
        y=y_train,
        hidden_size=args.hidden_size,
        num_layers=args.num_layers,
        epochs=args.epochs,
        learning_rate=args.learning_rate,
        figures_folder=args.figures_folder
    )
    model.fit()
    model.plot_loss()
    model.evaluate(X_test, y_test)
    logging.info(f'Saving model to {args.output_path}')
    with open(args.output_path, 'wb') as handle:
        pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    main()


