import pickle
import torch
from loaders import PandasGlobLoader
from util import plot_data
from train import Model
from preprocess import Preprocess
import matplotlib.pyplot as plt
import pandas as pd


def plot_august_01_prediction(df, preprocess, model, output_path, load_zone):
    timeframe_start = '2017-08-01 00:00:00'
    timeframe_end = '2017-08-02 00:00:00'
    lz = df[df['Name'] == load_zone]
    predictions = get_predictions_for_timeframe(df=lz, preprocess=preprocess, model=model,
                                                load_zone=load_zone, timeframe_start=timeframe_start)
    plot_predictions(df=lz, timeframe_start=timeframe_start, timeframe_end=timeframe_end, predictions=predictions,
                     output_path=output_path)


def get_predictions_for_timeframe(df, preprocess, model, load_zone, timeframe_start):
    df = df[(df['time_stamp'] < timeframe_start)]
    windowed = preprocess.run(df=df, window_size=48, load_zone=load_zone)
    t = torch.from_numpy(windowed).type(torch.Tensor)
    predicted = model.predict(t)
    # grab last 24 hours
    predictions = [item.item() for item in predicted[-24:]]
    return predictions


def plot_predictions(df, timeframe_start, timeframe_end, predictions, output_path):
    plt.figure(figsize=(32, 10))
    sliced = df[(df['time_stamp'] >= timeframe_start) & (df['time_stamp'] < timeframe_end)]
    plt.plot(sliced['time_stamp'], sliced['LBMP ($/MWHr)'])
    plt.plot(
        pd.date_range(timeframe_start, timeframe_end, freq="1h", inclusive='left'),
        predictions,
        color="red"
    )
    plt.legend(['Data', 'Prediction'])
    plt.xlabel('Datetime')
    plt.ylabel('LBMP')
    if output_path is None:
        plt.show()
    else:
        plt.savefig(output_path)
        plt.close()


def main():
    with open('./preprocessed/scaler.pickle', 'rb') as handle:
        scaler = pickle.load(handle)
    with open('./model/model.pickle', 'rb') as handle:
        predictive = pickle.load(handle)
    model = Model(X=None, y=None, predictive=predictive, scaler=scaler)
    df = PandasGlobLoader('./data/2017_NYISO_LBMPs/**/*.csv').load()
    preprocess = Preprocess(scaler=scaler)
    df = preprocess.encode_date(df)
    # Plots that help demonstrate key trends or patterns in the LBMP data
    plot_data(df=df, names=['N.Y.C.'], figsize=(32, 12), output_path='./figures/NYC.png')
    plot_data(df=df, figsize=(32, 12), output_path='./figures/NYC_EOY.png', include_dates=True,
              time_between=['2017-12-01 00:00', '2017-12-31 23:00'])
    plot_data(df=df, figsize=(32, 12), output_path='./figures/all.png')

    plot_august_01_prediction(df, preprocess, model, './figures/august_01.png', 'N.Y.C.')


if __name__ == '__main__':
    main()
