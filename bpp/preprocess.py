import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import logging
import argparse
from loaders import PandasGlobLoader
import pickle

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')


class Preprocess:
    def __init__(self, scaler):
        self.scaler = scaler

    def create_windows(self, df, window_size):
        df_np = df.to_numpy()
        windowed = []
        for i in range(len(df_np) - window_size + 1):
            window = df_np[i:i + window_size]
            windowed.append(window)
        return np.array(windowed)

    def encode_date(self, df):
        df = df.copy()
        df['time_stamp'] = pd.to_datetime(df['Time Stamp'], format='%m/%d/%Y %H:%M')
        return df

    def scale_data(self, df):
        data = df[['LBMP ($/MWHr)']]
        data['scaled_lbmp'] = self.scaler.fit_transform(data['LBMP ($/MWHr)'].values.reshape(-1, 1))
        y = data[['scaled_lbmp']]
        return y

    def run(self, df, window_size, load_zone):
        df = self.encode_date(df)
        df = df.sort_values(by=['time_stamp'])
        lz = df[df['Name'] == load_zone]
        scaled = self.scale_data(lz)
        windowed = self.create_windows(df=scaled, window_size=window_size)
        return windowed


def main():
    parser = argparse.ArgumentParser(description='Preprocessing')
    parser.add_argument('--data-path', help='Path to CSV files', required=False,
                        default='./data/2017_NYISO_LBMPs/**/*.csv')
    parser.add_argument('--preprocessed-output-path', help='Output path for preprocessed data', required=False,
                        default='./preprocessed/data.npy')
    parser.add_argument('--scaler-output-path', help='Output path for preprocessed data', required=False,
                        default='./preprocessed/scaler.pickle')
    parser.add_argument('--window-size', help='Size of window to create', required=False, default=48, type=int)
    parser.add_argument('--load-zone', help='Load zone to extract', required=False, default='N.Y.C.')
    args = parser.parse_args()
    loader = PandasGlobLoader(args.data_path)
    df = loader.load()
    logging.info(f'Processing {len(df)} items')
    min_max_scaler = MinMaxScaler(feature_range=(-1, 1))
    preprocess = Preprocess(scaler=min_max_scaler)
    preprocessed = preprocess.run(df, window_size=args.window_size, load_zone=args.load_zone)
    logging.info(f'Saving preprocessed data to {args.preprocessed_output_path}')
    np.save(args.preprocessed_output_path, preprocessed)
    logging.info(f'Saving scaler to {args.scaler_output_path}')
    with open(args.scaler_output_path, 'wb') as handle:
        pickle.dump(preprocess.scaler, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()
