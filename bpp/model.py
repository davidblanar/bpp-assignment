import torch
import torch.nn as nn


class GRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, output_size):
        super(GRU, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.gru = nn.GRU(input_size, hidden_size, num_layers, batch_first=True)
        self.linear = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        init_hidden_state = torch.zeros(self.num_layers, x.size(0), self.hidden_size).requires_grad_()
        output, (hn) = self.gru(x, (init_hidden_state.detach()))
        return self.linear(output[:, -1, :])
