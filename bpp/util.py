import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


def train_test_split(data, test_size):
    if test_size > 1 or test_size < 0:
        raise Exception('test_size should be between 0 and 1')
    test_len = round(len(data) * test_size)
    train_len = len(data) - test_len
    X_train = data[:train_len, :-1, :]
    y_train = data[:train_len, -1, :]
    X_test = data[train_len:, :-1]
    y_test = data[train_len:, -1, :]
    return X_train, X_test, y_train, y_test


def plot_data(df, figsize=None, names=None, time_between=None, include_dates=False, output_path=None):
    df = df.sort_values(by=['time_stamp'])
    if figsize is None:
        figsize = (64, 12)
    plt.figure(figsize=figsize)

    if time_between is not None:
        start, stop = pd.to_datetime(time_between[0]), pd.to_datetime(time_between[1])
        df = df[(df['time_stamp'] >= start) & (df['time_stamp'] <= stop)]

    if include_dates:
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        plt.gca().xaxis.set_major_locator(mdates.DayLocator())
        plt.gcf().autofmt_xdate()

    names = names if names is not None else list(df['Name'].unique())
    for name in names:
        part = df[df['Name'] == name]
        plt.plot(part['time_stamp'], part['LBMP ($/MWHr)'])
    plt.legend(names)
    plt.xlabel('Datetime')
    plt.ylabel('LBMP')
    if output_path is None:
        plt.show()
    else:
        plt.savefig(output_path)
        plt.close()

