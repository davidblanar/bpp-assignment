# BPP Assignment - Forecasting Electricity Prices

## Requirements
- Python 3
- Pip
- Dependencies listed in `requirements.txt`
- Jupyter Notebook (optional)

## Quick start
For ease of use an IPython notebook is provided which can be run using
[Jupyter Notebook](https://jupyter.org/). Simply load the file `bpp.ipynb` and run each cell to preprocess data,
train the model and run some predictions.

In case that is unfeasible, full installation can be used.

## Installation
Create and activate a virtual env (see [Pip Docs](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/) for help).

Install dependencies
`pip install -r requirements.txt`

## Run scripts locally
```
cd bpp
python3 preprocess.py
python3 train.py
python3 main.py
```
Each script can be provided with command line arguments to experiment with different
learning rates, epochs, as well as allowing for different input and output paths.

Each script contains sensible defaults in case no arguments are provided.

## Run tests
In root folder run

`pytest`

## Model description
The model selected is a multi-layer GRU (Gated Recurrent Unit) RNN.
GRUs offer efficient computation and are resilient to the Vanishing Gradient Problem.

The sole feature the model currently supports is LBMP prices. The selected learning rate, number of epochs, and GRU size were found to be
a good tradeoff between accuracy and speed of convergence.

## Notes and suggestions

#### Possible issues
- The pipeline as it is right now stores all data in memory. This could cause problems in case the amount of data increases over time. In that case a distributed tool such as [Dask](https://www.dask.org/) or [Spark](https://spark.apache.org/) can be used. The tooling used would heavily depend on the cloud environment in use.
- The data contains spikes, particularly around New Year's Eve which can cause problems as the model may fail to predict them accurately. The selected model seems to deal with this relatively well.
- As of right now the model takes into account only a single selected load zone. 

#### Deployment suggestions
The files can be built into a Docker container and deployed as an automated pipeline using an orchestrating tool
(e.g. [Airflow](https://airflow.apache.org/)) into a Kubernetes cluster in a cloud environment (AWS, GCP and such).


The files are structured such that each step (preprocessing, training, predictions)
can be performed separately. This allows us to deploy the scripts into a pipeline with isolated steps
so in case of faulty data or a bug in the implementation only the affected step needs
to be repeated.
    
      